
(function($){
	"use strict";
    $(document).on('ready',function(){

			//google API key
			$('#map').gmap3({
				address: "PT. Alter Trade Indonesia",
				zoom: 17,
				mapTypeId : google.maps.MapTypeId.ROADMAP
			})
			.marker([
				{position:[-7.421882, 112.715365]},
			]);

		});

})(jQuery);
