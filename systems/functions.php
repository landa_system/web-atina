<?php

function getLangList()
{
    return explode(',', str_replace(" ", "", getenv('LANG_LIST')));
}

function getLang()
{
    if(isset($_SESSION['lang'])){
        return $_SESSION['lang'];
    }else{
        return getenv('LANG_DEFAULT');
    }
}

function baseUrl($custom = NULL){
  $url = getenv('SITE_URL');
  if (isset($custom)) {
    return $url.$custom;
  } else {
    return $url;
  }
}

function contentLang($string)
{
    $lang = getLang();

    $path = 'lang/'.$lang.'.php';
    if(file_exists($path)){
        require $path;
        $list = $lang;
    }else{
        $list = [];
    }

    if(isset($list[$string])){
        return $list[$string];
    }else{
        return $string;
    }
}

function validate($data, $validasi, $custom = []) {
    if (!empty($custom)) {
        $validasiData = array_merge($validasi, $custom);
    } else {
        $validasiData = $validasi;
    }

    $validate = GUMP::is_valid($data, $validasiData);

    if ($validate === true) {
        return true;
    } else {
        return $validate;
    }
}

function sendBlastMail($subjek, $email_penerima, $content) {

    $db = new Cahkampung\Landadb(Db());
    $getEmail = $db->select("*")->from("m_setting")->where("id", "=", 1)->find();
    if (!empty($getEmail->email_smtp) && !empty($getEmail->password_smtp)) {

        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->SMTPDebug = 0;

        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
//        $mail->Username   = getenv('email_smtp');
//        $mail->Password   = getenv('password_smtp');
        $mail->Username = $getEmail->email_smtp;
        $mail->Password = $getEmail->password_smtp;
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;

        $mail->setFrom($getEmail->email_smtp, 'The Rich Prada Bali');

        foreach ($email_penerima as $key => $value) {
          $mail->addAddress($value);
        }

        $mail->isHTML(true);

        $mail->Subject = $subjek;
        $mail->Body = $content;

        if (!$mail->send()) {
            return [
                'status' => false,
//                'error'  => $mail->ErrorInfo,
            ];
        } else {
            return [
                'status' => true,
            ];
        }
    } else {
        return [
            'status' => false,
            'error' => 'Email SMTP Belum di setting',
        ];
    }
}

function sendMail($subjek, $pengirim, $pesan, $email) {

    $db = new Cahkampung\Landadb(Db());
    $getEmail = $db->select("*")->from("m_setting")->where("id", "=", 1)->find();
    if (!empty($getEmail->email_smtp) && !empty($getEmail->password_smtp)) {

        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->SMTPDebug = 0;

        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
//        $mail->Username   = getenv('email_smtp');
//        $mail->Password   = getenv('password_smtp');
        $mail->Username   = $getEmail->email_smtp;
        $mail->Password   = $getEmail->password_smtp;
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;

        $mail->setFrom($email, 'Website Alter Trade Indonesia');
        $mail->addAddress(getenv('EMAIL'), 'Customer Baru Alter Trade Indonesia');
        $mail->isHTML(true);

        $mail->Subject = 'Customer Baru Website Atina';
        $mail->Body = "<p> Pengirim : $pengirim </p> <br> <p style:font-size:18px> Email    :  $email</p> <br> <p> Pesan : $pesan</p>";

        if (!$mail->send()) {
            return [
                'status' => false,
                'error'  => $mail->ErrorInfo,
            ];
        } else {
            return [
                'status' => true,
            ];
        }
    } else {
        return [
            'status' => false,
            'error' => 'Email SMTP Belum di setting',
        ];
    }
}

function tanggal_indo($tanggal) {
    $bulan = array(1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $split = explode('-', $tanggal);
    return $split[2] . ' ' . $bulan[(int) $split[1]] . ' ' . $split[0];
}

function tanggal_indo_tgl_bln($tanggal) {
    $bulan = array(1 => 'Jan',
        'Feb',
        'Mar',
        'Apr',
        'Mei',
        'Jun',
        'Jul',
        'Agu',
        'Sep',
        'Okt',
        'Nov',
        'Des'
    );
    $split = explode('-', $tanggal);
    return $split[2] . ' ' . $bulan[(int) $split[1]];
}

function is_url($uri) {
    if (filter_var($uri, FILTER_VALIDATE_URL) === FALSE) {
        return false;
    } else {
        return true;
    }
}

function getImage($base64, $filename, $customename, $path = null) {
    if (isset($base64) and is_url($base64) == false) {
        $file = [
            'base64' => $base64,
            'filename' => $filename
        ];
        if (isset($path)) {
            $path_foto = $path;
        } else {
            $path_foto = './img/peserta';
        }
        $upload = base64ToFile($file, $path_foto, $customename);
        if ($upload['status'] == 1) {
            $return = $upload['fileName'];
        } else {
            $return = "";
        }
    } else {
        $return = "";
    }
    return $return;
}

function getImageDosen($base64, $filename, $customename) {
    if (isset($base64) and is_url($base64) == false) {
        $file = [
            'base64' => $base64,
            'filename' => $filename
        ];
        $upload = base64ToFile($file, 'app/img/dosen', $customename);
        if ($upload['status'] == 1) {
            $return = $upload['fileName'];
        } else {
            $return = "";
        }
    } else {
        $return = "";
    }
    return $return;
}

function random($length = 8) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ012345678";
    $password = substr(str_shuffle($chars), 0, $length);
    return $password;
}

function pagination($item_count, $limit, $cur_page, $link) {
    if ($limit > 0 && $item_count > 0) {
        $page_count = ceil($item_count / $limit);
        $paginate = '<div class="st-pagination" style="text-align:center"><ul class="pagination">';
        if ($page_count != 1) {
            if ($cur_page == 1) {
                $paginate .= '<li><a>First</a></li>';
            } else {
                $url = $link . addOrUpdateUrlParam('page', 1);
                $paginate .= '<li><a href="' . urldecode($url) . '">First</a></li>';
            }

            if ($cur_page > 1) {
                $url = $link . addOrUpdateUrlParam('page', $cur_page - 1);
                $paginate .= '<li><a href="' . urldecode($url) . '">Prev</a></li>';
            }

            if ($cur_page > 3) {
                $start = $cur_page - 2;
                $end = ($cur_page == $page_count) ? $page_count : (($page_count - $cur_page) + $cur_page);
            } else {
                $start = 1;
                $end = (($cur_page + 4) <= $page_count) ? ($cur_page + 4) : $page_count;
            }

            for ($i = $start; $i <= $end; $i++) {
                if ($i == $cur_page) {
                    $active = 'class="active"';
                } else {
                    $active = '';
                }

                $url = $link . addOrUpdateUrlParam('page', $i);

                $paginate .= '<li ' . $active . '><a href="' . urldecode($url) . '">' . $i . '</a></li>';
            }

            $url = $link . addOrUpdateUrlParam('page', $cur_page + 1);
            if ($cur_page != $page_count) {
                $paginate .= '<li><a href="' . urldecode($url) . '">Next</a></li>';
            }

            if ($cur_page == $page_count) {
                $paginate .= '<li><a>Last</a></li>';
            } else {
                $url = $link . addOrUpdateUrlParam('page', $page_count);
                $paginate .= '<li><a href="' . urldecode($url) . '">Last</a></li>';
            }

            $paginate .= '</ul></div>';
        } else {
            $paginate = '';
        }
        return $paginate;
    }
}

function cuplikan($str, $panjang) {
    return preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', substr(strip_tags($str), 0, $panjang)) . '...';
}

//fungsi baru
function gambar_pertama($string, $default = "img/no-images.svg") {
    preg_match('@<img.+src="(.*)".*>@Uims', $string, $matches);
    $src = $matches ? $matches[1] : getenv('SITE_URL').$default;
    return $src;
}


// get_first_image -> tidak rekomendasi digunakan
function get_first_image($html = NULL, $size = "big") {

    if (empty($class)) {
        $class = 'img img-responsive';
    } else {
        $class = $class;
    }

    $post_html = str_get_html($html);

    if ($post_html !== NULL) {

        $first_img = $post_html->find('img', true);

        $first_img->src = $first_img->src;

        if ($size == "small") {
            $first_img->src = str_replace('-700x700-', '-150x150-', $first_img->src);
        }

        if ($size == "medium") {
            $first_img->src = str_replace('-700x700-', '-350x350-', $first_img->src);
        }

        return $first_img->src;
    } else {
        return site_url() . "img/no-images.svg";
    }


}

function addOrUpdateUrlParam($name, $value) {
    $params = $_GET;
    unset($params[$name]);
    $params[$name] = $value;
    return '?' . http_build_query($params);
}

function indonesian_date($timestamp = '', $date_format = 'l, j F Y | H:i', $suffix = 'WIB') {
    if (trim($timestamp) == '') {
        $timestamp = time();
    } elseif (!ctype_digit($timestamp)) {
        $timestamp = strtotime($timestamp);
    }
    # remove S (st,nd,rd,th) there are no such things in indonesia :p
    $date_format = preg_replace("/S/", "", $date_format);
    $pattern = array(
        '/Mon[^day]/', '/Tue[^sday]/', '/Wed[^nesday]/', '/Thu[^rsday]/',
        '/Fri[^day]/', '/Sat[^urday]/', '/Sun[^day]/', '/Monday/', '/Tuesday/',
        '/Wednesday/', '/Thursday/', '/Friday/', '/Saturday/', '/Sunday/',
        '/Jan[^uary]/', '/Feb[^ruary]/', '/Mar[^ch]/', '/Apr[^il]/', '/May/',
        '/Jun[^e]/', '/Jul[^y]/', '/Aug[^ust]/', '/Sep[^tember]/', '/Oct[^ober]/',
        '/Nov[^ember]/', '/Dec[^ember]/', '/January/', '/February/', '/March/',
        '/April/', '/June/', '/July/', '/August/', '/September/', '/October/',
        '/November/', '/December/',
    );
    $replace = array('Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab', 'Min',
        'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu',
        'Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des',
        'Januari', 'Februari', 'Maret', 'April', 'Juni', 'Juli', 'Agustus', 'September',
        'Oktober', 'November', 'Desember',
    );
    $date = date($date_format, $timestamp);
    $date = preg_replace($pattern, $replace, $date);
    $date = "{$date} {$suffix}";
    $date = "{$date}";
    return $date;
}
function config($key, $value = null) {

    static $_config = array();

    if ($key === 'source' && file_exists($value))
        $_config = parse_ini_file($value, true);
    else if ($value == null)
        return (isset($_config[$key]) ? $_config[$key] : null);
    else
        $_config[$key] = $value;
}

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
     $ipaddress = getenv('HTTP_FORWARDED');
 else if(getenv('REMOTE_ADDR'))
    $ipaddress = getenv('REMOTE_ADDR');
else
    $ipaddress = 'UNKNOWN';
return $ipaddress;
}

function redirect_404(){
  $url = getenv('SITE_URL');
  header("Location:".$url.'404');
  die();
}

function contentParsing($string) {
    $regex = <<<'END'
/
  (
    (?: [\x00-\x7F]                 # single-byte sequences   0xxxxxxx
    |   [\xC0-\xDF][\x80-\xBF]      # double-byte sequences   110xxxxx 10xxxxxx
    |   [\xE0-\xEF][\x80-\xBF]{2}   # triple-byte sequences   1110xxxx 10xxxxxx * 2
    |   [\xF0-\xF7][\x80-\xBF]{3}   # quadruple-byte sequence 11110xxx 10xxxxxx * 3
    ){1,100}                        # ...one or more times
  )
| .                                 # anything else
/x
END;
return preg_replace($regex, '$1', $string);
}

function img_thumbnail($img, $type = NULL) {

  if (file_exists($img)) {
    return $img;
  } else {
    if ($type == "article") {
      return getenv('URL_IMG')."no-image-article.png";
    } elseif ($type == 'about') {
      return NULL;
    } else {
      return getenv('URL_IMG')."no-image.png";
    }
  }

}

function get_iframe($html)
{

    $result = preg_match_all('/\< *[img][^\>]*[src] *= *[\"\']{0,1}([^\"\']*)/i', $html, $match, PREG_SET_ORDER);
    $matches = array_pop($match);

    for ($i=0; $i < count($matches); $i++) {

        $img = $matches[$i].'"/>';

        $a = get_youtube($img);
        if ($a) {
           $iframe = '<div class="vid-news"><iframe width="100%" height="100%" src="'.$a[0].'">
        </iframe></div>';

        $yutub = str_replace($img, $iframe , $img);

        $konten = str_replace($img, $yutub, $html);

        $html = $konten;
        }
    }

   return $html;
}

function get_youtube($img)
{
    preg_match_all( '@ta-insert-video="([^"]+)"@' , $img, $match);

    $src = array_pop($match);

    return $src;
}
