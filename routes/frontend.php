<?php

$app->get('/', function ($request, $response, $args) {
  $db = $this->db;
  $config = $db->select("*")->from("m_setting")->find();

  $news = $db->select("DISTINCT('m_news.judul') judul, m_news.*")
    ->from('m_news')
    ->limit(4)
    ->join('left join', 'm_pengguna as p', 'm_news.created_by = p.id')
    ->where('m_news.is_deleted', '=', 0)
    ->orderBy('m_news.id DESC')
    ->findAll();

  foreach ($news as $key => $value) {
    $data_news[$key]['title']            = $value->judul;
    $data_news[$key]['readmore']         = cuplikan($value->content, 200);
    $data_news[$key]['alias_unique']     = baseUrl("news/" . date('Y/m/d', $value->jam) . '/' . $value->alias);

    $image = substr($value->gambar_thumb, -11);
    if ($image === "default.png") {
      $data_news[$key]['img']     = "img/no-image.png";
      // $data_news[$key]['img']     = str_replace("http://","https://",$data_news[$key]['img']); // conv http to https in image
    } else {
      $data_news[$key]['img']     = $value->gambar_thumb;
      // $data_news[$key]['img']     = str_replace("http://","https://",$data_news[$key]['img']); // conv http to https in image
    }
  }

  $banner = $db->select("*")->from("m_banner")->findAll();
  $countCar = 0;
  foreach ($banner as $key => $value) {
    $data_banner[$key]['img']        = $value->path;
    $temp_caption                    = explode(';', $value->posisi);
    $data_banner[$key]['caption']    = $temp_caption;
    $data_banner[$key]['numb']       = $countCar;
    $countCar++;
  }

  $highlight = $db->select("*")->from("m_produk")->limit(3)->where("kode", "=", "HIGHLIGHT")->andWhere("is_deleted", "=", 0)->orderBy('no_urut ASC')->findAll();
  foreach ($highlight as $key => $value) {
    // $value->gambar = str_replace("http://","https://",$value->gambar); // conv http to https in image
  }



  $seo_setting = array(
    'title'       => 'Homepage |' . $config->title,
    'description' => $config->description,
    'keywords'    => $config->keywords,
    'og_img'      => 'og_img',
    'url'         =>  baseUrl(),
  );
  $data = array(
    'seo_setting'     => $seo_setting,
    'page'            => 'home',
    'data_news'       => $data_news,
    'data_banner'     => $data_banner,
    'data_hightlight' => $highlight,
    'config'          => $config
  );
  return $this->view->render($response, 'views/index.html', $data);
});

$app->get('/ecoshrimp', function ($request, $response, $args) {
  $db = $this->db;
  $config = $db->select("*")->from("m_setting")->find();

  $page_ecoshrimp = $db->find("SELECT * FROM m_halaman WHERE tipe = 'ecoshrimp' ");

  // return successResponse($response, $page_ecoshrimp);

  $seo_setting = array(
    'title'       => 'Ecoshrimp',
    'description' => $config->description,
    'keywords'    => $config->keywords,
    'og_img'      => 'og_img',
    'url'         => baseUrl("ecoshrimp"),
  );
  $data = array(
    'page_title'      => "Ecoshrimp",
    'seo_setting'     => $seo_setting,
    'config'          => $config,
    'page_ecoshrimp'  => $page_ecoshrimp,
    'page'            => 'ecoshrimp'
  );
  return $this->view->render($response, 'views/ecoshrimp.html', $data);
});

$app->get('/about/{alias}', function ($request, $response, $args) {
  $db = $this->db;
  $page = $request->getAttribute('alias');
  $config = $db->select("*")->from("m_setting")->find();



  //   $page_about = $db->find("SELECT * FROM m_halaman WHERE tipe = '{$page}' ");

  // patch sqli model select 
  $array_list = [
    'corporate-info' => [
      'title' => 'Corporate Info',
      'select' => 'corporate-info'
    ],

    'vision-and-mission' => [
      'title' => 'Vision and Mission',
      'select' => 'vision-and-mission'
    ]
  ];


  $select_test = (isset($array_list[$page])) ? $array_list[$page] : ['title' => null, 'select' => null];


  $title = $select_test['title'];


  $page_about = $db->select("*")->from('m_halaman')->Where('tipe', '=', $select_test['select'])->find();

  $timeline = $db->select("DISTINCT('artikel.judul') judul, artikel.*")
    ->from('artikel')
    ->join('left join', 'm_pengguna as p', 'artikel.created_by = p.id')
    ->join('left join', 'kategori_artikel as kakel', 'artikel.id = kakel.artikel_id')
    ->join('left join', 'kategori as k', 'kakel.kategori_id = k.id')
    ->where('artikel.is_deleted', '=', 0)
    ->andWhere('k.alias', '=', 'history')
    ->orderBy('artikel.jam ASC')
    ->findAll();

  $page_timeline = array();
  foreach ($timeline as $key => $value) {
    $page_timeline[$key]['id']        = $value->id;
    $page_timeline[$key]['title']     = $value->judul;
    $page_timeline[$key]['content']   = cuplikan($value->content, 600);
    $page_timeline[$key]['tahun']     = date('Y', $value->jam);

    $image = substr($value->gambar_primary, -11);
    if ($image === "default.png") {
      $page_timeline[$key]['thumbnail']     = NULL;
    } else {
      $page_timeline[$key]['thumbnail']     = $value->gambar_primary;
    }
  }

  // return successResponse($response, $page_timeline);

  $seo_setting = array(
    'title'       => $title,
    'description' => 'About | ' . $title,
    'keywords'    => $config->keywords,
    'og_img'      => 'og_img',
    'url'         => baseUrl($page),
  );

  $data = array(
    'page_title'      => $title,
    'page_about'      => $page_about,
    'page_timeline'   => $page_timeline,
    'seo_setting'     => $seo_setting,
    'config'          => $config,
    'page'            => 'about'
  );
  return $this->view->render($response, 'views/' . $page . '.html', $data);
});

$app->get('/products', function ($request, $response, $args) {
  $db = $this->db;
  $config = $db->select("*")->from("m_setting")->find();

  if (!empty($_GET['tipeproduk'])) {
    $filter = $_GET['tipeproduk'];
  } else {
    $filter = 0;
  }

  if (!empty($filter)) {

    $int_var = (int)filter_var($filter, FILTER_SANITIZE_NUMBER_INT);

    $models = $db->findAll("SELECT * FROM m_produk WHERE kode = 'PRODUK' AND tipeproduk_id = $int_var AND is_deleted = 0 ORDER BY no_urut ASC ");

    $selected = $db->select("*")->from("m_tipeproduk")->Where('id', '=', $int_var)->findAll();
  } else {
    $models = $db->findAll("SELECT * FROM m_produk WHERE kode = 'PRODUK' AND is_deleted = 0 ORDER BY no_urut ASC ");
    $selected = "";
  }

  $idxView = 1;
  $return = [];
  if (!empty($models)) {
    foreach ($models as $key => $val) {

      $return[$key] = (array) $val;
      $pengguna     = $db->select("nama")
        ->from('m_pengguna')
        ->where('id', '=', $val->created_by)
        ->find();

      $return[$key]['img']    = $val->gambar;
      $return[$key]['creator'] = isset($pengguna->nama) ? $pengguna->nama : '';
      $return[$key]['date_mod']    = date('M d, Y', $val->modified_at);
      $return[$key]['alias_unique'] = $val->alias;
      $return[$key]['idx_view'] = $idxView;
      $idxView += 1;
    }
  } else {
    $return = '';
  }

  $listtipe = $db->select("*")->from("m_tipeproduk")->findAll();

  $seo_setting = array(
    'title'       => 'Products',
    'description' => $config->description,
    'keywords'    => $config->keywords,
    'og_img'      => 'og_img',
    'url'         => 'url',
  );
  $data = array(
    'page_title'      => "Products",
    'seo_setting'     => $seo_setting,
    'data_products'   => $return,
    'list_tipeproduk' => $listtipe,
    'selected'        => $selected,
    'page'            => 'products',
    'filter'          => $filter
  );
  return $this->view->render($response, 'views/products.html', $data);
});

$app->get('/products/{alias}', function ($request, $response, $args) {
  $db = $this->db;

  $config = $db->select("*")->from("m_setting")->find();
  $data = $db->select("*")->from("m_produk")->where('m_produk.alias', '=', $request->getAttribute('alias'))->find();
  $models = $db->select("*")->from("m_produk")->where('m_produk.alias', '=', $request->getAttribute('alias'))->findAll();

  $return = [];
  if (!empty($models)) {
    foreach ($models as $key => $val) {

      $return[$key] = (array) $val;
      $pengguna     = $db->select("nama")
        ->from('m_pengguna')
        ->where('id', '=', $val->created_by)
        ->find();

      $return[$key]['img']          = $val->gambar;
      $return[$key]['judul']        = $val->nama;
      $return[$key]['creator']      = isset($pengguna->nama) ? $pengguna->nama : '';
      $return[$key]['date']         = date('M d, Y', $val->modified_at);
      $return[$key]['alias_unique'] = $val->alias;
      $return[$key]['arrival']      = date('d M, Y', $val->arrival);
      $return[$key]['stock']        = $val->stock;
      $return[$key]['harga']        = number_format($val->harga, 2, ',', '.');
      $return[$key]['harga_diskon'] = $val->harga_diskon;
    }
  } else {
    $return = '';
  }

  // return successResponse($response, $return);

  $seo_setting = array(
    'title'       => $data->nama,
    'description' => $data->nama,
    'keywords'    => $config->keywords,
    'og_img'      => $data->gambar,
    'url'         => baseUrl($data->id),
  );
  $data = array(
    'seo_setting'     => $seo_setting,
    'product_detail'  => $return,
    'page'            => 'blog'
  );
  return $this->view->render($response, 'views/products-detail.html', $data);
});

$app->get('/news', function ($request, $response, $args) {
  $db = $this->db;
  $req = $request->getParams();

  if (!empty($req['page'])) {
    $page = (int) filter_var($req['page'], FILTER_SANITIZE_NUMBER_INT);
  } else {
    $page = 0;
  }

  $config = $db->select("*")->from("m_setting")->find();
  $db->select("DISTINCT('m_news.judul') judul, m_news.*")
    ->from('m_news')
    ->limit(12)
    ->join('left join', 'm_pengguna as p', 'm_news.created_by = p.id')
    ->where('m_news.is_deleted', '=', 0)
    ->orderBy('m_news.id DESC');

  /** Set offset */
  if (!empty($page)) { 
    $db->offset((12*($page-1)));
  }

  $models = $db->findAll();
  $totalItem = $db->count();

  //pagination 
  $calculate = (int) (ceil($totalItem / 12) == 0) ? 1 : ceil($totalItem / 12);
  $number = range(1, $calculate);

  $array = [];
  foreach ($number as $key => $value) {
    $array[$key]['numb'] = $value;

    if ($page == $value) {
      $array[$key]['status'] = 'active';
    } elseif ($page == 0 && $value == 1) {
      $array[$key]['status'] = 'active';
    } else {
      $array[$key]['status'] = '';
    }
  }

  $return = [];
  if (!empty($models)) {
    foreach ($models as $key => $val) {

      $return[$key] = (array) $val;
      $pengguna     = $db->select("nama")
        ->from('m_pengguna')
        ->where('id', '=', $val->created_by)
        ->find();

      $tgl = date('d/M/Y H:i', $val->jam);
      $now = date('d/M/Y H:i');
      $return[$key]['content'] = contentParsing($val->content);
      $return[$key]['jam']     = $tgl;
      // $return[$key]['img']     = img_thumbnail($val->gambar_thumb, "article");
      $return[$key]['img']    = $val->gambar_thumb;
      $return[$key]['creator'] = isset($pengguna->nama) ? $pengguna->nama : '';
      $return[$key]['pub']     = $val->publish == '1' && $tgl >= $now || $val->publish == '0' ? '0' : '1';
      $return[$key]['date']    = date('M d, Y', $val->jam);
      $return[$key]['alias_unique'] = date('Y/m/d', $val->jam) . '/' . $val->alias;
      $return[$key]['cuplikan'] = cuplikan($val->content, 230);
      $return[$key]['kategori'] = "News";
    }
  } else {
    $return = '';
  }

  // return successResponse($response, $return);

  $seo_setting = array(
    'title'       => 'News & Event',
    'keywords'    => $config->keywords,
    'og_img'      => 'og_img',
    'url'         => baseUrl("news"),
  );

  $data = array(
    'page_title'  => "News & Event",
    'seo_setting' => $seo_setting,
    'page'          => 'news',
    'list_artikel'  => $return,
    'pagination' => [
      'disableprev' => ($page == 1 OR $page == 0) ? 'disabled' : '',
      'disablenext' => ($page == max($number)) ? 'disabled' : '',
      'onpage' => ($page == 0) ? 1 : $page,
      'arr_age' => $array
    ]

  );
  return $this->view->render($response, 'views/news.html', $data);
});

$app->get('/news/{y}/{m}/{d}/{alias}', function ($request, $response, $args) {
  $db = $this->db;

  $data = $db->select("*")->from("m_news")->where('m_news.alias', '=', $request->getAttribute('alias'))->find();

  $models = $db->select("DISTINCT('m_news.judul') judul, m_news.*")
    ->from('m_news')
    ->join('left join', 'm_pengguna as p', 'm_news.created_by = p.id')
    ->where('m_news.alias', '=', $request->getAttribute('alias'))
    ->orderBy('m_news.id DESC')
    ->findAll();

  $return = [];
  if (!empty($models)) {
    foreach ($models as $key => $val) {

      $return[$key] = (array) $val;
      $pengguna     = $db->select("nama")
        ->from('m_pengguna')
        ->where('id', '=', $val->created_by)
        ->find();

      $tgl = date('d/M/Y H:i', $val->jam);
      $now = date('d/M/Y H:i');
      // $return[$key]['content'] = contentParsing($val->content);
      $return[$key]['video']   = get_iframe("$val->content");
      $return[$key]['jam']     = $tgl;
      $return[$key]['creator'] = isset($pengguna->nama) ? $pengguna->nama : 'Admin';
      $return[$key]['pub']     = $val->publish == '1' && $tgl >= $now || $val->publish == '0' ? '0' : '1';
      $return[$key]['date']    = date('M d, Y', $val->jam);
      $return[$key]['alias_unique'] = date('Y/m/d', $val->jam) . '/' . $val->alias;

      $image = substr($val->gambar_primary, -11);
      if ($image === "default.png") {
        $return[$key]['img']     = $val->gambar_primary;
      } else {
        $return[$key]['img']     = NULL;
      }

      $return[$key]['kategori'] = "News";
    }
  } else {
    $return = '';
  }

  $models_recent = $db->select("DISTINCT('m_news.judul') judul, m_news.*")
    ->from('m_news')
    ->limit(6)
    ->join('left join', 'm_pengguna as p', 'm_news.created_by = p.id')
    ->where('m_news.is_deleted', '=', 0)
    ->andWhere('m_news.id', '<>', $data->id)
    ->orderBy('m_news.id DESC')
    ->findAll();

  $data_artikel = [];
  if (!empty($models_recent)) {
    foreach ($models_recent as $key => $val) {

      $data_artikel[$key] = (array) $val;
      $pengguna     = $db->select("nama")
        ->from('m_pengguna')
        ->where('id', '=', $val->created_by)
        ->find();

      $tgl = date('d/M/Y H:i', $val->jam);
      $now = date('d/M/Y H:i');
      $data_artikel[$key]['content'] = contentParsing($val->content);
      $data_artikel[$key]['jam']     = $tgl;
      // $return[$key]['img']     = img_thumbnail($val->gambar_thumb, "article");
      $data_artikel[$key]['img']    = $val->gambar_thumb;
      $data_artikel[$key]['creator'] = isset($pengguna->nama) ? $pengguna->nama : '';
      $data_artikel[$key]['pub']     = $val->publish == '1' && $tgl >= $now || $val->publish == '0' ? '0' : '1';
      $data_artikel[$key]['date']    = date('M d, Y', $val->jam);
      $data_artikel[$key]['alias_unique'] = $val->alias;
      $data_artikel[$key]['cuplikan'] = cuplikan($val->content, 230);
      $data_artikel[$key]['kategori'] = "News";
    }
  } else {
    $data_artikel = '';
  }

  // return successResponse($response, $return);

  $seo_setting = array(
    'title'       => $data->judul,
    'keywords'    => $data->keyword,
    'og_img'      => $data->gambar_thumb,
    'url'         => baseUrl($data->alias),
  );
  $data = array(
    'seo_setting'     => $seo_setting,
    'artikel_detail'  => $return,
    'page'            => 'blog',
    'data_news'       => $data_artikel
  );
  return $this->view->render($response, 'views/news-detail.html', $data);
});

$app->get('/contact', function ($request, $response, $args) {

  $db = $this->db;
  $config = $db->select("*")->from("m_setting")->find();


  // return successResponse($response, $config);
  $seo_setting = array(
    'title'       => 'Contact',
    'description' => $config->description,
    'keywords'    => $config->keywords,
    'og_img'      => 'og_img',
    'url'         => baseUrl("contact"),
  );
  $data = array(
    'page_title'  => "Contact Us",
    'seo_setting' => $seo_setting,
    'page'        => 'contact',
    'config'      => $config
  );
  return $this->view->render($response, 'views/contact.html', $data);
});

$app->post('/send-message', function ($request, $response, $args) {
  $params = $request->getParams();
  // return successResponse($response, $params);

  header('location: ' . baseUrl());
  die();
});

$app->post('/savedata.html', function ($request, $response) {
  $req = $request->getParams();

  if (!empty($req['name']) && !empty($req['message']) && !empty($req['email'])) {
    /** Kirim Email */
    $send = sendMail('CUSTOMER BARU', $req['name'], $req['message'], $req['email']);
    /** End */
    if ($send['status'] == true) {
      $status = 1;
    } else {
      $status = 2;
    }
  } else {
    $status = 3;
  }
  return $response->withRedirect(getenv('SITE_URL') . '?status=' . $status);
})->setName('savedata');
